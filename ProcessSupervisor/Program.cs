﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.IO;

class TopedInstance
{
    public TopedInstance(string topologie)
    {
        this.__topologie = topologie;
    }

    public void Start()
    {
        this.__process = Process.Start(this.__topologie);
    }

    public delegate void ProcessKillHandler();

    public event ProcessKillHandler ProcessKilled;

    private void OnProcessKilled()
    {
        if (ProcessKilled != null) {
            ProcessKilled();
        }
    }

    private void SupervisorThread()
    {
        int filelength = 0;
        while (true)
        {
            Thread.Sleep(60 * 1000);
            var file = new FileInfo(this.__jsonLog);
            if (file.Length == filelength)
            {
                this.__process.Kill();
                File.Move(this.__jsonLog, this.__jsonLog + DateTime.Now.ToShortTimeString());
            }
        }
    }

    private Process __process;
    private String __topologie;
    private String __jsonLog = @"e:\elp\carmen\trace.json";
}

namespace ProcessSupervisor
{
    class Program
    {
        static void Main(string[] args)
        {
            TopedInstance toped = new TopedInstance(args[0]);
            toped.Start();
        }
    }
}
